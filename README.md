Šifra ADFGX a ADFGVX

Úkol:
Libovolný text obsahující jakékolik písmena, čísla, mezery a speciální znaky upravit. 
Vyřadit speciální znaky.
Odstraní diakritku a převede na velká písmena.
Mezery nahradí za 'XMEZERAX'
Cisla pro ADFGX nahradí za 'XCISLOX'

Dékoduje zadaný text.
Enkóduje zadaný text.
Vypíše upravenou zadanou zprávu, klíčové slovo, tajnou abecedu, enkódovanou a zpětně dekódovanou zprávu.



Více informací o tom jak šifra funguje: http://soutez2005.crypto-world.info/images/ADFGX.pdf



