import random

alphabet_adfgvx = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
alphabet_cz = list('ABCDEFGHIJKLMNOPQRSTUVXYZ') # without W
alphabet_en = list('ABCDEFGHIKLMNOPQRSTUVWXYZ') # without J

input_message = input('Enter message to encode: ')
input_word = input('Enter keyword: ')
lang = input('Choose way to encrypt: "cz", "en" or "adfgvx": ')

number_list = '0123456789'

NORM_STR = 'Ě ŠČŘŽÝÁÍÉÚŮŤĎŇÓ'
NORM_DICT = {
    'Ě' :   'E',
    ' ' :   'XMEZERAX',
    'Š' :   'S',
    'Č' :   'C',
    'Ř' :   'R',
    'Ž' :   'Z',
    'Ý' :   'Y',
    'Á' :   'A',
    'Í' :   'I',
    'É' :   'E',
    'Ú' :   'U',
    'Ů' :   'U',
    'Ť' :   'T',
    'Ď' :   'D',
    'Ň' :   'N',
    'Ó' :   'O'
}

def normalized_message(input_message, lang):
    '''Text adjustment for message and remove specials characters'''
    text = input_message.upper()

    non_diacritic = ''
    for letter in text:
        if letter in NORM_STR:
            non_diacritic = non_diacritic + (NORM_DICT[letter])
        else:
            non_diacritic = non_diacritic + letter

    while True:
        if lang == 'cz':
            alphabet = alphabet_cz
            non_diacritic = non_diacritic.replace('W', 'V')
            break
        elif lang == 'en':
            alphabet = alphabet_en
            non_diacritic = non_diacritic.replace('J', 'I')
            break
        elif lang == 'adfgvx':
            alphabet = alphabet_adfgvx
            break
        else:
            print('ERROR, choose one of choice in quotation marks!')
            lang = input('Choose way to encrypt: "cz", "en" or "adfgvx": ')

    random.shuffle(alphabet)
    secret_alphabet = ''.join(alphabet)
    print('Secret alphabet is:',secret_alphabet)

    normalized_text = []

    # write new text without others characters
    for element in non_diacritic:
        if element in alphabet_adfgvx:
            normalized_text.append(element)

    new_text = []
    if lang != 'adfgvx':
        for c in normalized_text:
            if c == ' ':
                c = 'XMEZERAX'
                new_text.append(c)
            elif c not in number_list:
                new_text.append(c)
            elif c in number_list:
                c = 'XCISLOX' + str(chr(ord(c)+27))
                new_text.append(c)

        normalized_text = ''.join(new_text)
        print('Normalized text is:', normalized_text)
    return(normalized_text, secret_alphabet,lang)

normalized_text, secret_alphabet, lang = normalized_message(input_message, lang)



def normalized_enter_key(input_word):
    '''Text adjustment for keyword and remove specials characters'''

    input_word = input_word.replace(' ', '')
    input_word = list(input_word.upper())

    non_diacritic = ''
    for letter in input_word:
        if letter in NORM_STR:
            non_diacritic = non_diacritic + (NORM_DICT[letter])
        else:
            non_diacritic = non_diacritic + letter

    normalized_input_word = []
    for element in non_diacritic:
        if element in alphabet_adfgvx:
            normalized_input_word.append(element)

    without_multiple_character = []
    for pismenko in normalized_input_word:
        if pismenko not in without_multiple_character:
            without_multiple_character.append(pismenko)

    return without_multiple_character

modified_keyword = normalized_enter_key(input_word)

if lang != 'adfgvx':
    magic = 'ADFGVX'
else:
    magic = 'ADFGX'

#encode
def encode(normalized_text,secret_alphabet):
    '''encode'''
    encoding = []
    for c in normalized_text:
        row, col = divmod(secret_alphabet.index(c), len(magic))
        encoding = encoding + [magic[row], magic[col]]


    word = list(modified_keyword)
    print('Secret keyword is:', ''.join(word))
    number_of_columns = len(word)

    ordered = sorted(range(number_of_columns),key = lambda i: word[i]) # [1, 0, 3, 4, 2]
    encode_sifra = ''.join(encoding[j] for i in ordered for j in range(i, len(encoding), number_of_columns))
    return (encode_sifra, word)

encode_sifra, keyword = encode(normalized_text,secret_alphabet)
print('Encode cipher: ',encode_sifra)

#decode
def decode_cipher():
    '''decode'''
    word = []
    for c in keyword:
        if c not in word: word.append(c)

    key_length = len(word)
    k = sorted(range(key_length), key=lambda i: word[i])

    # reorder index
    # len(encode_sifra) == 16 ->
    # x = [0, 6, 12, 4, 10, 3, 9, 15, 1, 7, 13, 2, 8, 14, 5, 11]
    message_length = len(encode_sifra)
    x = [j for i in k for j in range(i, message_length, key_length)]

    # reorder
    # encode_sifra = 'FXGAFVXXAXDDDXGA' ->
    # y = ['F','A','D','V','A','G','X','X','D','X','F','A','G','D','X','X']
    number_of_columns = ['']*message_length
    for i, c in zip(x, encode_sifra): number_of_columns[i] = c

    sec_alp = list(secret_alphabet)

    decoding = []
    for i in range(0, message_length, 2):
        row, col = number_of_columns[i:i+2]
        decoding.append(sec_alp[len(magic) * magic.index(row) + magic.index(col)])
    decode_cipher = ''.join(decoding)
    decode_cipher = decode_cipher.replace('XMEZERAX', ' ')

    for i in range(75,84+1):
        decode_cipher = decode_cipher.replace('XCISLOX' +  str(chr(i)), str(chr(i-27)))
    # XCISLOXK = 0 ---> chr(75) = K ... chr(75-27) = 0
    # XCISLOXO = 9 --->
    return (decode_cipher)


print('Decode cipher: ',decode_cipher())
